const mongoose = require('mongoose');

const TrackSchema = new mongoose.Schema({
    track: {
        type: String,
        required: true
    },
    album: mongoose.Schema.Types.ObjectId,
    duration: String
});

const Track = mongoose.model('Track', TrackSchema);
module.exports = Track;