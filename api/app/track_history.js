const express = require('express');
const User = require('../models/User');
const TrackHistory = require('../models/TrackHistory');
const router = express.Router();

router.post('/', async (req, res) => {
    if (!req.body.track) {
        return res.status(400).send({error: 'You have not put track'});
    }

    const token = req.get('Authorization');

    if (!token) {
        return res.status(401).send({error: 'No token present'});
    }

    const user = await User.findOne({token});
    if(!user) {
        return res.status(401).send({error: 'Wrong token'});
    }

    const trackHistoryData = {
        user,
        track: req.body.track,
        datetime: new Date()
    };

    const trackHistory = new TrackHistory(trackHistoryData);
    try {
        await trackHistory.save();
        res.send(trackHistory);
    }
    catch (e) {
        res.status(400).send({error: 'Data not valid.'})
    }
});

module.exports = router;