const express = require('express');
const router = express.Router();
const Track = require('../models/Track');

router.get ('/', async (req, res) => {
    const query = {};

    if (req.query.album) {
        query.album = req.query.album;
    }

    try {
        const tracks = await Track.find(query);
        res.send(tracks);
    }
    catch (e) {
        res.sendStatus(500);
    }
});

router.post ('/', async (req, res) => {
    if (!req.body.track) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const trackData = {
        track: req.body.track,
        album: req.body.album || null,
        duration: req.body.duration
    };

    const track = new Track(trackData);
    try {
        await track.save();
        res.send(track);
    }
    catch (e) {
        res.status(400).send({error: 'Data not valid'});
    }
});

module.exports = router;