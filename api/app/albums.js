const express = require('express');
const router = express.Router();
const Album = require('../models/Album');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    const query = {};

    if (req.query.artist) {
        query.artist = req.query.artist;
    }

    try {
        const albums = await Album.find(query);
        res.send(albums);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const album = await Album.findById(req.params.id).populate('artist', 'name');
        if (album) {
            res.send(album);
        } else {
            res.status(404).send({error: 'Album not found'});
        }
    }
    catch (e) {
        res.sendStatus(500);
    }
})

router.post('/', upload.single('image'), async (req, res) => {
    if (!req.body.title || !req.body.artist) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const albumData = {
        title: req.body.title,
        artist: req.body.artist,
        issueYear: req.body.issueYear || null
    };

    if (req.file) {
        albumData.image = req.file.image
    }

    const album = new Album(albumData);

    try {
        await album.save();
        res.send(album);
    } catch (e) {
        res.status(400).send({error: 'Smth went wrong'});
    }
})

module.exports = router;